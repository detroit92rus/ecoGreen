import "./style.scss";
import { Card } from "../../components/card/index";
import { Header } from "../../components/header/index";
import { CardSmall } from "../../components/card-small/index";
import { Footer } from "../../components/footer/index";
import { Banner } from "../../components/banner/index";
import { News } from "../../components/news/index";
import { CardBig } from "../../components/card-big/index";
import { cards, cardsSmall, cardsBig } from "./data";

import rectangle15 from "./assets/image/rectangle15.png";

export const MainPage = () => {
  return (
    <div>
      <Header />
      <div className="main-page">
        <div className="mb-6 flex justify-center flex-col ">
          <h2 className="font-bold pt-6 pb-4 pl-2 text-gray-900">
            Recommended for you
          </h2>
          <div className="flex justify-around w-76 gap-x-4 overflow-x-auto scrollbar-hidden">
            {cardsSmall.map((e) => (
              <CardSmall title={e.title} imageUrl={e.imageUrl} key={e.id} />
            ))}
          </div>
        </div>
        <Banner />
        <div className="mt-8">
          <div className="flex justify-between mb-4">
            <p className="font-bold text-gray-900	" id="plants">
              Plants and flowers
            </p>
            <a href="#plants" className="text-dark-green font-bold">
              See all
            </a>
          </div>
          <div className="main-page__catalog">
            {cards.map((e) => (
              <Card
                title={e.title}
                imageUrl={e.imageUrl}
                rating={e.rating}
                key={e.id}
              />
            ))}
          </div>
        </div>
        <div className="mt-6 w-76">
          <div className="flex justify-between  mb-2 ">
            <p className="font-bold flex" id="plant">
              Plant care
            </p>
            <a href="#plant" className="text-dark-green font-bold ">
              See all
            </a>
          </div>
          <div className="flex gap-x-4 overflow-x-auto scrollbar-hidden">
            {cardsBig.map((e) => (
              <CardBig
                imgUrl={e.imgUrl}
                description={e.description}
                title={e.title}
                key={e.id}
              />
            ))}
          </div>
        </div>
        <div className="my-6 w-76">
          <div className="flex justify-between mb-2">
            <p className="font-bold text-gray-900" id="news">
              News
            </p>
            <a href="#news" className="text-blue font-bold">
              See all
            </a>
          </div>
          <News
            imgUrl={rectangle15}
            title="Indoor flowers and potted plants"
            date="16.06.21"
            views="16"
          />
        </div>
      </div>
      <Footer />
    </div>
  );
};
