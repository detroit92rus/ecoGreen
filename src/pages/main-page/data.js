import orchid from "./assets/image/orchid.svg";
import pions from "./assets/image/pions.svg";
import aloe from "./assets/image/aloe.svg";
import chrysanthemum from "./assets/image/chrysanthemum.svg";
import obi from "../../components/card-small/assets/image/obi.svg";
import lerua from "../../components/card-small/assets/image/lerua.svg";
import imageBox from "./assets/image/imageBox.png";

export const cardsSmall = [
  {
    id: 1,
    title: "ОБИ",
    imageUrl: obi,
  },
  {
    id: 2,
    title: "Леруа Марлен",
    imageUrl: lerua,
  },
  {
    id: 3,
    title: "ОБИ",
    imageUrl: obi,
  },
  {
    id: 4,
    title: "Леруа Марлен",
    imageUrl: lerua,
  },
  {
    id: 5,
    title: "ОБИ",
    imageUrl: obi,
  },
  {
    id: 6,
    title: "Леруа Марлен",
    imageUrl: lerua,
  },
];
export const cards = [
  {
    id: 1,
    title: "Orchid",
    imageUrl: orchid,
    rating: "8.0",
  },
  {
    id: 2,
    title: "Pions",
    imageUrl: pions,
    rating: "9.0",
  },
  {
    id: 3,
    title: "Aloe",
    imageUrl: aloe,
    rating: "5.6",
  },
  {
    id: 4,
    title: "Chrysanthemum",
    imageUrl: chrysanthemum,
    rating: "7.5",
  },
];
export const cardsBig = [
  {
    id: 1,
    imgUrl: imageBox,
    title: "Pots for plants",
    description: "Flower pot Ingreen ø13 h10. 4 cm v0. 7 L plastic marble",
  },
  {
    id: 2,
    imgUrl: imageBox,
    title: "Pots for plants",
    description: "Flower pot Ingreen ø13 h10. 4 cm v0. 7 L plastic marble",
  },
  {
    id: 3,
    imgUrl: imageBox,
    title: "Pots for plants",
    description: "Flower pot Ingreen ø13 h10. 4 cm v0. 7 L plastic marble",
  },
  {
    id: 4,
    imgUrl: imageBox,
    title: "Pots for plants",
    description: "Flower pot Ingreen ø13 h10. 4 cm v0. 7 L plastic marble",
  },
];
