import { MainPage } from "./pages/main-page/index";

function App() {
  return (
    <div>
      <MainPage />
    </div>
  );
}

export default App;
