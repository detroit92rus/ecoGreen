import "./style.scss";
import icon from "./assets/image/sign-up-icon.svg";

export const Menu = ({ toggleMenu }) => {
  return (
    <div className="menu">
      <button className="menu__sign-up">
        <img src={icon} alt="login-icon" className="menu__sign-up-icon" />
        Sign Up or Log in
      </button>
      <div className="menu__navigation">
        <div className="menu__nav-link-wrapper">
          <a href="#plants" className="menu__nav-link" onClick={toggleMenu}>
            Plants and flowers
          </a>
        </div>

        <div className="menu__nav-link-wrapper">
          <a href="#plant" className="menu__nav-link" onClick={toggleMenu}>
            Plant care
          </a>
        </div>
        <div className="menu__nav-link-wrapper">
          <a href="#news" className="menu__nav-link" onClick={toggleMenu}>
            News
          </a>
        </div>
      </div>
    </div>
  );
};
