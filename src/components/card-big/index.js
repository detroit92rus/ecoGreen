export const CardBig = ({ imgUrl, title, description }) => {
  return (
    <div className="flex flex-col w-60 h-64 p-2 bg-white rounded-lg shadow-md">
      <img src={imgUrl} alt="title" className="w-full h-31 rounded-lg" />
      <div className="flex flex-col items-start	my-2 w-56 h-15">
        <span className="text-lime-600 font-normal">{title}</span>
        <p className="text-sm text-gray-900 font-semibold">{description}</p>
      </div>
      <button className="bg-dark-green text-white rounded-sm py-1 px-8 my-2 ">
        Go over
      </button>
    </div>
  );
};
