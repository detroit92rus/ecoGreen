import flower from "./assets/image/flower.svg";
import "./style.scss";

export const Banner = () => {
  return (
    <div className="flex  bg-banner-color rounded-lg  max-w-max p-4 justify-center ">
      <div className="flex flex-col w-56 h-21">
        <p className="font-semibold text-white">
          Rubber-bearing ficus (elastic)
        </p>
        <span className="text-transparent-white ellipses-2">
          Ficus elastica (Ficus elastica), or rubber-bearing ficus Ficusssss
          elastica (Ficus elastica), or rubber-bearing ficus Ficus elastica
          (Ficus elastica), or rubber-bearing ficus
        </span>
      </div>
      <img src={flower} alt="flower" />
    </div>
  );
};
