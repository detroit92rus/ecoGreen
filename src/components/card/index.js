import star from "./assets/image/star.svg";

export const Card = ({ rating, imageUrl, title }) => {
  return (
    <div className="w-36 h-50 shadow-md overflow-hidden rounded-lg bg-white">
      <div className="bg-gray-900 flex justify-center">
        <img src={imageUrl} alt="flower" />
      </div>
      <div className="flex flex-col items-center">
        <div className="flex w-11 bg-yellow-500 rounded-sm -mt-2 h-4 justify-around ">
          <img src={star} alt="star" />
          <span className="text-xs text-white">{rating}</span>
        </div>
        <div className="mb-3">
          <p className="font-bold my-2">{title}</p>
        </div>
        <button className="bg-light-lime text-dark-green rounded-lg py-1 px-8 my-2">
          Review
        </button>
      </div>
    </div>
  );
};
