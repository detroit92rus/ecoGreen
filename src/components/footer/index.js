import age from "./assets/image/icon-age.svg";

export const Footer = () => {
  return (
    <footer className="flex justify-center p-4 gap-4 h-30 bg-gray-900">
      <img src={age} alt="age" className="w-12 h-12" />
      <span className="text-gray-500 font-medium text-base text-xs">
        <p>
          Our website is intended for informational purposes only and is not an
          advertisement.
        </p>
        <p>© 2018 - 2021</p>
      </span>
    </footer>
  );
};
