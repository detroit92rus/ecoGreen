import eye from "./assets/image/eye.svg";

export const News = ({ imgUrl, title, date, views }) => {
  return (
    <div>
      <img src={imgUrl} alt="rectangl15" />
      <div className="flex flex-col">
        <span className="font-bold text-gray-900 pl-1">{title}</span>
        <div className="flex flex-row pl-1">
          <span className="text-gray-500">{date}</span>
          <div className="flex  ml-2 items-center ">
            <img src={eye} alt="eye" />
            <span className="text-gray-500 text-sm ml-1">{views}</span>
          </div>
        </div>
      </div>
    </div>
  );
};
