import { useCallback, useState } from "react";
import "./style.scss";
import menu from "./assets/image/menu.svg";
import logo from "./assets/image/logo.svg";
import search from "./assets/image/search.svg";
import selectLanguage from "./assets/image/pt-language.svg";
import { Menu } from "../menu/index";

export const Header = () => {
  const [isOpenMenu, setIsOpenMenu] = useState(false);
  const toggleMenu = useCallback(() => {
    setIsOpenMenu(!isOpenMenu);
  }, [isOpenMenu]);
  return (
    <header className="header px-24">
      <img src={logo} alt="logo" className="header__logo" />
      <div className="header__actions">
        <img src={search} alt="search" className="header__search" />
        <img
          src={selectLanguage}
          alt="select-language"
          className="header__select-language"
        />
        <img
          src={menu}
          alt="burger"
          className="header__menu"
          onClick={toggleMenu}
        />
      </div>
      {isOpenMenu && <Menu toggleMenu={toggleMenu} />}
    </header>
  );
};
