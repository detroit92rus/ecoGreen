export const CardSmall = ({ title, imageUrl }) => {
  return (
    <div className="flex flex-col items-center w-32 h-32 ">
      <div className="rounded-lg w-32 h-20 bg-white shadow-md flex items-center justify-center ">
        <img src={imageUrl} alt="logo" />
      </div>
      <span className="text-center mt-2 font-semibold text-base h-9">
        {title}
      </span>
    </div>
  );
};
